package main

import (
	"american-shorthair/handler"
	"fmt"
	"log"
	"os"

	"github.com/labstack/echo"

	"github.com/line/line-bot-sdk-go/linebot"
)

func main() {
	startService()
}

func startService() {
	fmt.Println("american shorthair is running")
	e := echo.New()
	handler.NewServiceHandler(e, connectLineBot())
	e.Logger.Fatal(e.Start(":3000"))
}

func connectLineBot() *linebot.Client {
	fmt.Println(os.Getenv("CHANNEL_TOKEN"))
	bot, err := linebot.New(
		os.Getenv("CHANNEL_SECRET"),
		os.Getenv("CHANNEL_TOKEN"),
	)
	fmt.Println(bot)
	if err != nil {
		log.Fatal(err)
	}
	return bot
}
