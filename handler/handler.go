package handler

import (
	"context"
	"fmt"
	"log"

	"github.com/labstack/echo"
	"github.com/line/line-bot-sdk-go/linebot"
)

type HttpCallbackHandler struct {
	Bot         *linebot.Client
	ServiceInfo string
}

func NewServiceHandler(e *echo.Echo, linebot *linebot.Client) {
	e.GET("/", func(c echo.Context) error {
		return c.String(200, "Hi, I am linebot")
	})
	handlers := &HttpCallbackHandler{
		Bot:         linebot,
		ServiceInfo: "test",
	}
	e.POST("/", handlers.Callback)
}

func (handler *HttpCallbackHandler) Callback(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}
	events, err := handler.Bot.ParseRequest(c.Request())
	if err != nil {
		if err == linebot.ErrInvalidSignature {
			c.String(400, linebot.ErrInvalidSignature.Error())
		} else {
			c.String(500, "internal server error")
		}
	}

	for _, event := range events {
		fmt.Println("event: ", event)
		if event.Type == linebot.EventTypeMessage {
			switch event.Message.(type) {
			case *linebot.TextMessage:
				messageFromPing := "Meaww!! Hi from american shorthair."
				if _, err = handler.Bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage(messageFromPing)).Do(); err != nil {
					log.Print(err)
				}
			case *linebot.StickerMessage:
				messageFromPing := "Meaww....Mmmmmmmm"
				if _, err = handler.Bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage(messageFromPing)).Do(); err != nil {
					log.Print(err)
				}
			}
		}
	}
	return c.JSON(200, "")
}
