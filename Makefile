run:
	go run main.go

cover:
	go test -cover ./... -coverprofile=cover.out
	go tool cover -html=cover.out -o coverage.html

test:
	go test -v ./...

bench:
	go test -benchmem -run= ./... -bench .

build:
	docker image build -t xmlparser -f Dockerfile .
	docker container run --rm xmlparser
